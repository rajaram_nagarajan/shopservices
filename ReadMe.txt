Name: Shop service

Purpose: To store the list of Shops in memory and find the nearest shop based on the user's geo location (latitude, longitude)

Services exposed:

1) Path - /shop-service/v1/shops
Method - POST
Request Object - Shop

Example:
URL: http://localhost:8080/shop-service/v1/shops
Method: POST
Headers: Content-Type: application/json
Input Data:

{
"name" : "shop3",
"address" : {
"number" : "3",
"postCode" : "IG117PX"
}
}

Output Data:

{"name":"shop3","address":{"number":3,"postCode":"IG117PX"},"geoLocation":{"longitude":0.0802855,"latitude":51.537031}}


2) Path - /shop-service/v1/shops
Method - GET
Request Object - NA
Request params - latitude, longitude

Example:

URL: http://localhost:8080/shop-service/v1/shops?latitude=51.4830485&longitude=0.317399
Method: GET
Headers: Content-Type: application/json

Output Data:

{"name":"shop3","address":{"number":3,"postCode":"IG117PX"},"geoLocation":{"longitude":0.0802855,"latitude":51.537031}}

-------------------------------------------------------------------------------------

Code setup:

After downloading the code, run the following commands in the source directory

1. apply plugin: 'java'

2. gradle wrapper --gradle-version 2.13 command to create wrapper

3. ./gradlew build command to build the project

4. ./gradlew test command to run the tests


Or the ShopControllerTest.java can be started directly

Starting the application:

The Application.java file can be started directly and the APIs can be accessed using Chrome Postman or something similar