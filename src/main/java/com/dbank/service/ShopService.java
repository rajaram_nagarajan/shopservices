package com.dbank.service;

import com.dbank.dto.GeoLocation;
import com.dbank.dto.Shop;
import com.dbank.exception.InvalidPostCodeException;
import com.dbank.exception.ShopNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
* This class is the brain of the system which does all the shop related operations
* */
public class ShopService {

    private List<Shop> shopList = new ArrayList<>();

    private static Logger logger = LoggerFactory.getLogger(ShopService.class);

    // TODO All the constants can be moved to class level

    /*
    * Checks whether the post code is supplied or not
    * */
    public boolean isPostCodePresent(Shop shop) {
        if (shop != null && shop.getAddress() != null && shop.getAddress().getPostCode() != null) {
            return true;
        }

        logger.error("Post Code is null");
        return false;
    }

    /*
    * Adds or updates the given shop into memory
    * */
    public Shop addShop(Shop shop) throws Exception {
        GeoLocation geoLocation = findGeoLocation(shop.getAddress().getPostCode());
        shop.setGeoLocation(geoLocation);

        long replacedShopCount = shopList.stream().filter(s -> s.equals(shop)).map(s -> s.replace(shop)).count();
        if (replacedShopCount <= 0) {
            shopList.add(shop);
            logger.info("Shop {} has been added successfully", shop.getName());
        } else {
            logger.info("Shop {} has been updated successfully", shop.getName());
        }

        return shopList.stream().filter(s -> s.equals(shop)).findFirst().get();
    }

    /*
    * Finds the geo location of the shop based on it's post code
    * throws invalid post code exception if the post code is not a valid one
    * */
    private GeoLocation findGeoLocation(String postCode) throws Exception
    {
        // TODO Geo location can be found based on an entire valid address instead of just the post code to improve accuracy
        String api = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + URLEncoder.encode(postCode, "UTF-8") + "&sensor=true";

        URL url = new URL(api);
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        httpConnection.connect();

        if (httpConnection.getResponseCode() == 200) {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(httpConnection.getInputStream());
            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile("/GeocodeResponse/status");
            String status = (String) expr.evaluate(document, XPathConstants.STRING);
            if (status.equals("OK")) {
                expr = xpath.compile("//geometry/location/lat");
                String latitude = (String) expr.evaluate(document, XPathConstants.STRING);
                expr = xpath.compile("//geometry/location/lng");
                String longitude = (String) expr.evaluate(document, XPathConstants.STRING);
                GeoLocation geoLocation = new GeoLocation(Double.parseDouble(longitude), Double.parseDouble(latitude));
                return geoLocation;
            } else {
                logger.error("Post Code {} is invalid", postCode);
                throw new InvalidPostCodeException(postCode);
            }
        }

        logger.error("Couldn't find Geo Location for Post Code: {}", postCode);
        return null;
    }

    /*
    * Finds the nearest shop for the given latitude and longitude of the user's current location
    * throws shop not found exception if there is no near by shop
    * */
    public Shop findNearestShop(Double userLatitude, Double userLongitude) {
        // TODO List of shops can be returned back instead of just a single shop, so that user will have options to choose
        Map<Shop, Double> shopDistances = new HashMap<>();

        shopList.forEach(shop -> {
            Double distance = null;
            GeoLocation geoLocation = shop.getGeoLocation();
            if (geoLocation != null) {
                distance = findDistance(
                        geoLocation.getLatitude(), geoLocation.getLongitude(), userLatitude, userLongitude);
            }
            shopDistances.put(shop, distance);
        });

        Map.Entry<Shop, Double> min = null;
        for (Map.Entry<Shop, Double> entry : shopDistances.entrySet()) {
            if (min == null || min.getValue() > entry.getValue()) {
                min = entry;
            }
        }

        if (min == null) {
            logger.error("No nearest shop found for user's latitude {} and longitude {}", userLatitude, userLongitude);
            throw new ShopNotFoundException(userLatitude, userLongitude);
        }

        Shop nearestShop = min.getKey();
        logger.info("Nearest shop {} found for user's latitude {} and longitude {}", nearestShop.getName(), userLatitude, userLongitude);
        return shopList.stream().filter(s -> s.equals(nearestShop)).findFirst().get();
    }

    /*
    * Finds the distance between the user's location and the shop's location using Haversine's formula
    * Returns the distance in miles
    * */
    private double findDistance(double shopLatitude, double shopLongitude, double userLatitude, double userLongitude) {
        // TODO Instead of straight line distance calculation, the driving distance can be implemented to account the obstacles and find the real nearby shops
        double earthRadius = 6371000;
        double dLat = Math.toRadians(userLatitude-shopLatitude);
        double dLng = Math.toRadians(userLongitude-shopLongitude);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(shopLatitude)) * Math.cos(Math.toRadians(userLatitude)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double dist = earthRadius * c;

        return dist * 0.000621;
    }

}

