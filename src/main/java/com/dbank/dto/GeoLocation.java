package com.dbank.dto;

/*
* This class maintains the shop's geographical location
* */
public class GeoLocation {

    private Double longitude;

    private Double latitude;

    public GeoLocation() { }

    public GeoLocation(Double longitude, Double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "geoLocation: { longitude: " + longitude + ", "
                + "latitude: " + latitude + "}";
    }
}
