package com.dbank.dto;

/*
* This class maintains the shop's address details
* */
public class Address {
    private int number;

    private String postCode;

    public Address() { }

    public Address(int number, String postCode) {
        this.number = number;
        this.postCode = postCode;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Override
    public String toString() {
        return "address: { number: " + number + ", "
                + "postCode: " + postCode + "}";
    }
}
