package com.dbank.dto;

import java.util.Objects;

/*
* This class maintains the entire shop details
* */
public class Shop {
    // name is the unique identifier for the shop
    private String name;

    private Address address;

    private GeoLocation geoLocation;

    public Shop() {}

    public Shop(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    @Override
    public String toString() {
        return "shop: { name: " + name + ", "
                + address + ", "
                + geoLocation + "}";
    }

    /*
    * The shop objects will be compared based on it's name
    * */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }

        if (!(that instanceof Shop)) {
            return false;
        }

        Shop thatShop = (Shop) that;
        if (Objects.equals(this.name, thatShop.name)) {
            return true;
        }

        return false;
    }

    /*
    * The shop address will be replaced with all the details other than it's name
    * */
    public boolean replace(Shop that) {
        if (this == that) {
            return true;
        }

        if (!(that instanceof Shop)) {
            return false;
        }

        this.setAddress(that.getAddress());
        this.setGeoLocation(that.getGeoLocation());

        return true;
    }
}
