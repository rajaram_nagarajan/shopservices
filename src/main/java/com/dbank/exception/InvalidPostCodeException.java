package com.dbank.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidPostCodeException extends RuntimeException {
    public InvalidPostCodeException(String postCode) {
        super("Post Code: '" + postCode + "' is invalid.");
    }
}
