package com.dbank.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ShopNotFoundException extends RuntimeException {
    public ShopNotFoundException(Double latitude, Double longitude) {
        super("Nearest Shop not found for latitude " + latitude + ", longitude " + longitude + ".");
    }
}
