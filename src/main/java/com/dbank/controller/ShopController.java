package com.dbank.controller;

import com.dbank.dto.Shop;
import com.dbank.exception.InternalServerErrorException;
import com.dbank.exception.InvalidPostCodeException;
import com.dbank.exception.ShopNotFoundException;
import com.dbank.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/*
* This is the REST controller class defines the Shop APIs
* */
// TODO Swagger annotations can be used to provide an user interface
@RestController()
@RequestMapping(value = "/shop-service/v1/shops")
public class ShopController {

    @Autowired
    ShopService shopService;

    /*
    * The latitude and longitude value of the users'current location will be passed to find the nearest shop
    * returns the shop not found exception
    * */
    @RequestMapping(method = RequestMethod.GET)
    public Shop findNearByShop(@RequestParam("latitude") String latitude, @RequestParam("longitude") String longitude) {
        try {
            Double userLatitude = Double.parseDouble(latitude);
            Double userLongitude = Double.parseDouble(longitude);
            return shopService.findNearestShop(userLatitude, userLongitude);
        } catch (ShopNotFoundException ex) {
            throw ex;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new InternalServerErrorException();
        }
    }

    /*
    * The shop name and address will be provided to find the geo location and shop them in memory
    * returns invalid post code exception
    * */
    @RequestMapping(method = RequestMethod.POST)
    public Shop addOrUpdateShop (@RequestBody Shop shop) {
        try {
            if (shopService.isPostCodePresent(shop)) {
                return shopService.addShop(shop);
            } else {
                throw new InvalidPostCodeException(null);
            }
        } catch (InvalidPostCodeException ex) {
            throw ex;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new InternalServerErrorException();
        }
    }
}
