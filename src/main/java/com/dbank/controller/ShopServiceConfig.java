package com.dbank.controller;

import com.dbank.service.ShopService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
* The application bean supplier
* */
@Configuration
public class ShopServiceConfig {

    @Bean
    public ShopService shopService() {
        return new ShopService();
    }
}
