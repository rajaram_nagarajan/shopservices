package com.dbank.controller;

import com.dbank.dto.Address;
import com.dbank.dto.Shop;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class ShopControllerTest {

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void shopWorkflow() throws Exception {
        // Test for no nearby shops
        mockMvc.perform(get("/shop-service/v1/shops?latitude=12345&longitude=45789"))
                .andExpect(status().isNotFound());

        Address address = new Address(34, "RM176JT");
        Shop shop = new Shop("Shop1", address);

        // Test for adding a brand new shop
        mockMvc.perform(post("/shop-service/v1/shops")
                .content(json(shop))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Shop1")))
                .andExpect(jsonPath("$.address.number", is(34)))
                .andExpect(jsonPath("$.address.postCode", is("RM176JT")))
                .andExpect(jsonPath("$.geoLocation.latitude", is(51.4730599)))
                .andExpect(jsonPath("$.geoLocation.longitude", is(0.3256017)));

        // Test for updating the existing shop
        shop.getAddress().setPostCode("IG117PX");
        mockMvc.perform(post("/shop-service/v1/shops")
                .content(json(shop))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Shop1")))
                .andExpect(jsonPath("$.address.number", is(34)))
                .andExpect(jsonPath("$.address.postCode", is("IG117PX")))
                .andExpect(jsonPath("$.geoLocation.latitude", is(51.537031)))
                .andExpect(jsonPath("$.geoLocation.longitude", is(0.0802855)));

        Address address1 = new Address(190, "RM176JT");
        Shop shop1 = new Shop("Shop2", address1);

        // Test for adding a brand new shop
        mockMvc.perform(post("/shop-service/v1/shops")
                .content(json(shop1))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Shop2")))
                .andExpect(jsonPath("$.address.number", is(190)))
                .andExpect(jsonPath("$.address.postCode", is("RM176JT")))
                .andExpect(jsonPath("$.geoLocation.latitude", is(51.4730599)))
                .andExpect(jsonPath("$.geoLocation.longitude", is(0.3256017)));

        // Test for a valid nearby shop
        mockMvc.perform(get("/shop-service/v1/shops?latitude=51.4730000&longitude=0.3256000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Shop2")))
                .andExpect(jsonPath("$.address.number", is(190)))
                .andExpect(jsonPath("$.address.postCode", is("RM176JT")))
                .andExpect(jsonPath("$.geoLocation.latitude", is(51.4730599)))
                .andExpect(jsonPath("$.geoLocation.longitude", is(0.3256017)));
    }

   @Test
   public void invalidPostCode() throws Exception {
       Address address = new Address(567, "RM176JTIG117PX");
       Shop shop = new Shop("Shop2", address);

       // Test for adding an invalid shop
       mockMvc.perform(post("/shop-service/v1/shops")
               .content(json(shop))
               .contentType(contentType))
               .andExpect(status().isBadRequest());
   }

    @Test
    public void postCodeWithNullValue() throws Exception {
        Address address = new Address(567, null);
        Shop shop = new Shop("Shop2", address);

        // Test for adding a shop with post code value as null
        mockMvc.perform(post("/shop-service/v1/shops")
                .content(json(shop))
                .contentType(contentType))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shopWithNullValue() throws Exception {
        Shop shop = null;

        // Test for adding a shop with null value
        mockMvc.perform(post("/shop-service/v1/shops")
                .content(json(shop))
                .contentType(contentType))
                .andExpect(status().isBadRequest());
    }

    private String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
}
